Audit SSH
============================
This ruleset will perform SSH audit

## Description
This ruleset runs dev-sec ssh baseline audit

* ![DevSec SSH Baseline](https://github.com/dev-sec/ssh-baseline "Inspec profile github link")

## Hierarchy



## Required variables with no default

**None**


## Required variables with default

**None**


## Optional variables with default

### `AUDIT_SSL_PROFILES_ALERT_LIST`:
  * description: 
  * default: ssl-baseline

## Tags
1. Audit
1. Best Practices

## Categories


## Diagram


## Icon


