coreo_agent_selector_rule 'check-linux' do
    action :define
    timeout 120
    control 'check-linux' do
        describe os.linux? do
            it { should eq true }
        end
    end
end

coreo_agent_audit_profile 'ssl-baseline' do
    action :define
    selectors ['check-linux']
    profile 'https://github.com/dev-sec/ssh-baseline/archive/master.zip'
    timeout 120
end

coreo_agent_rule_runner 'audit-ssl-profiles' do
    action :run
    profiles ${AUDIT_SSL_PROFILES_ALERT_LIST}
    filter(${FILTERED_OBJECTS}) if ${FILTERED_OBJECTS}
end
  